import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';
import 'firebase_options.dart';
import 'package:flutter/material.dart';
import 'package:whacamole/screens/game_screen.dart';
import 'package:whacamole/screens/home_screen.dart';
import 'package:whacamole/screens/create_game.dart';
import 'package:whacamole/screens/join_game.dart';

Future <void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  
  runApp(
   Provider.value(
     value: FirebaseFirestore.instance,
     child: const MyApp(),
   ),
 );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: const Color.fromARGB(255, 13, 59, 11)),
        useMaterial3: true,
      ),
      home: const HomeScreen(),
      routes: {
        '/createGame': (context) => const CreateGame(),
        '/joinGame': (context) =>  const JoinGame(),
        '/startGame': (context) => const StartGame(),
      },
    );
  }
}