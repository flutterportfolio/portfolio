import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:synchronized/synchronized.dart';

class StartGame extends StatefulWidget {
  const StartGame({super.key});

  @override
  _StartGameState createState() => _StartGameState();
}

class _StartGameState extends State<StartGame> {
  Color _backgroundColor = Colors.red;
  late Timer _timer;
  int _score = 0;
  String _playerName = '';

  // Firestore setup
  final CollectionReference lobbiesCollection =
      FirebaseFirestore.instance.collection('lobbies');
  final CollectionReference leaderboardCollection =
      FirebaseFirestore.instance.collection('leaderboard');

  @override
  void initState() {
    super.initState();
    _startColorChange();
    _initFirestore();
  }

  void _initFirestore() async {
    // Check if the document exists, if not, create it
    DocumentSnapshot document = await lobbiesCollection.doc('lobby').get();
    if (!document.exists) {
      await lobbiesCollection.doc('lobby').set({'overallScore': 0});
    }

    // Listen for changes in the Firestore collection
    lobbiesCollection.doc('lobby').snapshots().listen((snapshot) {
      if (snapshot.exists) {
        setState(() {
          _score = snapshot['overallScore'];
        });
      }
    });
  }

  final _mutex = Lock();

  void _startColorChange() async {
    await _mutex.synchronized(() async {
      await Future.delayed(Duration(seconds: Random().nextInt(6)));

      _timer = Timer.periodic(const Duration(milliseconds: 800), (timer) {
        setState(() {
          _backgroundColor = _backgroundColor == Colors.red
              ? Colors.green
              : Colors.red;
        });
      });
    });
  }

  void _onTap() {
    if (_backgroundColor == Colors.green) {
      setState(() {
        _score += 1;
      });
    } else if (_backgroundColor == Colors.red) {
      _showGameOverDialog();
    }
  }

  void _showGameOverDialog() {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('Game over'),
          content: Column(
            children: [
              TextField(
                decoration: const InputDecoration(labelText: 'Enter your name'),
                onChanged: (value) {
                  setState(() {
                    _playerName = value;
                  });
                },
              ),
              ElevatedButton(
                onPressed: () {
                  _addToLeaderboard();
                },
                child: const Text('Submit Score'),
              ),
              ElevatedButton(
                onPressed: () {
                  _restartGame();
                  Navigator.of(context).pop(); 
                },
                child: const Text('Restart Game'),
              ),
              _buildLeaderboard(),
            ],
          ),
        );
      },
    );
  }

  void _addToLeaderboard() {
    if (_playerName.isNotEmpty) {
      leaderboardCollection.add({
        'name': _playerName,
        'score': _score,
      });
    }
  }

  void _restartGame() {
    lobbiesCollection.doc('lobby').update({'overallScore': 0});
    setState(() {
      _score = 0;
    });
  }

  Widget _buildLeaderboard() {
    return StreamBuilder<QuerySnapshot>(
      stream: leaderboardCollection.orderBy('score', descending: true).snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }

        var leaderboardData = snapshot.data?.docs ?? [];
        
        return Column(
          children: [
            const Text('Leaderboard'),
            for (var entry in leaderboardData)
              ListTile(
                title: Text('${entry['name']} - ${entry['score']}'),
              ),
          ],
        );
      },
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Whac-A-Mole - Score: $_score'),
      ),
      body: GestureDetector(
        onTap: _onTap,
        child: Container(
          color: _backgroundColor,
          child: Center(
            child: StreamBuilder(
              stream: lobbiesCollection.doc('lobby').snapshots(),
              builder: (context, AsyncSnapshot<DocumentSnapshot> snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const CircularProgressIndicator();
                }
                return const SizedBox.shrink();
              },
            ),
          ),
        ),
      ),
    );
  }
}
