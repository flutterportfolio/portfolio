import 'package:flutter/material.dart';
import 'package:whacamole/screens/create_game.dart'; 
import 'package:whacamole/screens/join_game.dart';  

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const CreateGame()),
                );
              },
              child: const Text('Create game'),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const JoinGame()),
                );
              },
              child: const Text('Join game'),
            ),
          ],
        ),
      ),
    );
  }
}
