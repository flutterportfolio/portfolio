import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:whacamole/screens/waiting_lobby.dart';

class CreateGame extends StatefulWidget {
  const CreateGame({super.key});

  @override
  _CreateGameState createState() => _CreateGameState();
}

class _CreateGameState extends State<CreateGame> {
  final TextEditingController _lobbyNameController = TextEditingController();
  final TextEditingController _userNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Create Game'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(11.11),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              controller: _lobbyNameController,
              decoration: const InputDecoration(labelText: 'Lobby Name'),
            ),
            TextField(
              controller: _userNameController,
              decoration: const InputDecoration(labelText: 'Username'),
            ),
            ElevatedButton(
              onPressed: () async {
                await createGameLobby(); // Make the method call asynchronous
              },
              child: const Text('Create Lobby'),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> createGameLobby() async {
    final String lobbyName = _lobbyNameController.text.trim();
    final String userName = _userNameController.text.trim();

    // Ensure lobbyName is not empty
    if (lobbyName.isNotEmpty) {
      try {
        // Set the document ID to be the same as the lobbyName
        await FirebaseFirestore.instance
            .collection('lobbies')
            .doc(lobbyName)
            .set({
              'lobbyName': lobbyName,
              'host': userName,
              'members': FieldValue.arrayUnion([userName]),
              'gameStarted': false,
              'overallScore': 0,
            });

        List<String> members = [userName];

        // Navigate to the WaitingLobby page
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => WaitingLobby(
              lobbyId: lobbyName, // Use the lobbyName as the lobbyId
              lobbyName: lobbyName,
              host: userName,
              members: members,
              overallScore: 0, // Initial overall score
            ),
          ),
        );
      } catch (error) {
        // Handle errors (e.g., show an error message)
        print('Error creating lobby: $error');
      }
    } else {
      // Handle invalid input (e.g., show an error message)
    }
  }
}
