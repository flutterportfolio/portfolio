import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:whacamole/screens/waiting_lobby.dart';

class JoinGame extends StatefulWidget {
  const JoinGame({super.key});

  @override
  State<JoinGame> createState() => _JoinGameState();
}

class _JoinGameState extends State<JoinGame> {
  final TextEditingController _lobbyNameController = TextEditingController();
  final TextEditingController _userNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Join Game'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(11.11),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              decoration: const InputDecoration(labelText: 'Lobby Name'),
              controller: _lobbyNameController,
            ),
            TextField(
              decoration: const InputDecoration(labelText: 'Username'),
              controller: _userNameController,
            ),
            ElevatedButton(
              onPressed: () {
                joinLobby(context);
              },
              child: const Text('Join Lobby'),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> joinLobby(BuildContext context) async {
    final String lobbyName = _lobbyNameController.text.trim();
    final String userName = _userNameController.text.trim();

    if (lobbyName.isNotEmpty) {
      // Perform a query to find the lobby by name in the 'room' collection
      QuerySnapshot querySnapshot = await FirebaseFirestore.instance
          .collection('lobbies')
          .where('lobbyName', isEqualTo: lobbyName)
          .limit(1)
          .get();

      if (querySnapshot.docs.isNotEmpty) {
        // Lobby exists, join the lobby
        DocumentSnapshot lobbySnapshot = querySnapshot.docs.first;

        // Update the 'members' field by adding the new member
        await lobbySnapshot.reference.update({
          'members': FieldValue.arrayUnion([userName]),
        });

        // Retrieve the updated lobby information
        DocumentSnapshot updatedLobbySnapshot =
            await lobbySnapshot.reference.get();

        // Retrieve the list of members from the updated lobby
        List<String> members =
            List<String>.from(updatedLobbySnapshot['members']);

        // Navigate to the WaitingLobby page
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => WaitingLobby(
              lobbyId: lobbySnapshot.id,
              lobbyName: lobbySnapshot['lobbyName'],
              host: lobbySnapshot['host'],
              members: members,
              overallScore: const ['overallScore'],
            ),
          ),
        );
      } else {
        print('Lobby does not exist');
      }
    }
  }
}
