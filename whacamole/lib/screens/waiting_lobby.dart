import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class WaitingLobby extends StatefulWidget {
  final String lobbyId;
  final String lobbyName;
  final String host;
  final List<String> members;

  const WaitingLobby({
    super.key,
    required this.lobbyId,
    required this.lobbyName,
    required this.host,
    required this.members, required overallScore,
  });

  @override
  _WaitingLobbyState createState() => _WaitingLobbyState();
}

class _WaitingLobbyState extends State<WaitingLobby> {
  late StreamSubscription<QuerySnapshot> _lobbyStreamSubscription;

  @override
  void initState() {
    super.initState();

    // Set up a stream subscription to listen for changes in the 'lobbies' collection
    _lobbyStreamSubscription = FirebaseFirestore.instance
        .collection('lobbies')
        .snapshots()
        .listen((QuerySnapshot snapshot) {
          // Handle changes in the collection
          // You may want to update the UI or take other actions based on the changes
          // For example, check the 'gameStarted' field in each document
          for (var document in snapshot.docs) {
            Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
            if (data['lobbyName'] == widget.lobbyName) {
              // Check if 'gameStarted' is true and perform actions accordingly
              bool gameStarted = data['gameStarted'];
              if (gameStarted) {
                // Navigate to the '/startGame' page or perform other actions
                Navigator.pushNamed(context, '/startGame', arguments: widget.lobbyId);
              }
            }
          }
        });
  }

  @override
  void dispose() {
    // Cancel the stream subscription when the widget is disposed
    _lobbyStreamSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: FirebaseFirestore.instance.collection('lobbies').snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return const Text('Something went wrong');
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Text('Loading');
        }

        return Scaffold(
          appBar: AppBar(
            title: const Text('Waiting Lobby'),
          ),
          body: ListView(
            children: snapshot.data!.docs.map((DocumentSnapshot document) {
              Map<String, dynamic> data = document.data()! as Map<String, dynamic>;

              // Check if 'members' field exists and is a List
              if (data.containsKey('members') && data['members'] is List) {
                List<dynamic> membersList = data['members'];

                // Check if 'members' list contains strings
                if (membersList.isNotEmpty && membersList.first is String) {
                  String membersString = membersList.join(', ');

                  return ListTile(
                    title: Text(data['lobbyName']),
                    subtitle: Text('Members: $membersString'),
                    trailing: data['gameStarted'] == true
                        ? null // Game started, don't show the button
                        : ElevatedButton(
                            onPressed: () {
                              startGame(context, data);
                            },
                            child: const Text('Start Game'),
                          ),
                  );
                }
              }

              return ListTile(
                title: Text(data['lobbyName']),
                subtitle: const Text('Members data format not recognized'),
              );
            }).toList(),
          ),
        );
      },
    );
  }
}

void startGame(BuildContext context, Map<String, dynamic> lobbyData) {
  // Check if the game has already started
  if (lobbyData['gameStarted'] != true) {
    // Update 'gameStarted' field in Firestore
    FirebaseFirestore.instance
        .collection('lobbies')
        .doc('lobby') 
        .update({'gameStarted': true})
        .then((_) {
          // Navigate to the '/startGame' page
          Navigator.pushNamed(
            context,
            '/startGame',
            arguments: lobbyData,
          );
        })
        .catchError((error) {
          // Handle errors if any
          print('Error updating gameStarted: $error');
        });
  } else {
    // The game has already started, handle accordingly
    print('Game has already started');
  }
}
